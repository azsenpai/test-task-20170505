<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use app\models\LoginForm;
use app\models\RegisterForm;
use app\models\TransferForm;
use app\models\User;
use app\models\BalanceHistory;

/**
 *
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'transfer', 'balance-history'],
                'rules' => [
                    [
                        'actions' => ['logout', 'transfer', 'balance-history'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $query = User::find();

        if (Yii::$app->user->id) {
            $query->where(['!=', 'id', Yii::$app->user->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     *
     */
    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new RegisterForm();

        if ($model->load(Yii::$app->request->post()) && $model->register()) {
            return $this->goBack();
        }

        return $this->render('register', [
            'model' => $model,
        ]);
    }

    /**
     *
     */
    public function actionTransfer()
    {
        $model = new TransferForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->transfer()) {
                Yii::$app->session->setFlash('transfer-success');
            } else {
                Yii::$app->session->setFlash('transfer-error');
            }

            return $this->redirect(['transfer']);
        }

        return $this->render('transfer', [
            'model' => $model,
        ]);
    }

    /**
     *
     */
    public function actionBalanceHistory()
    {
        $user_id = Yii::$app->user->id;

        $dataProvider = new ActiveDataProvider([
            'query' => BalanceHistory::find()->where([
                'OR',
                ['from_user_id' => $user_id],
                ['to_user_id' => $user_id],
            ]),
        ]);

        return $this->render('balance-history', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
