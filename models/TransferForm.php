<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 *
 */
class TransferForm extends Model
{
    public $username;
    public $sum;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'sum'], 'required'],
            ['username', 'compare', 'compareValue' => Yii::$app->user->identity->username, 'operator' => '!='],

            ['sum', 'number'],
            ['sum', 'filter', 'filter' => 'doubleval'],
            ['sum', 'compare', 'compareValue' => 0, 'operator' => '>', 'type' => 'number'],
        ];
    }

    /**
     *
     */
    public function transfer()
    {
        if ($this->validate()) {
            $user = Yii::$app->user->identity;

            return $user->transfer($this->username, $this->sum);
        }

        return false;
    }
}
