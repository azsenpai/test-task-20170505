<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Transaction;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property double $balance
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property BalanceHistory[] $balanceHistories
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     *
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['balance'], 'number'],
            [['created_at', 'updated_at'], 'integer'],
            [['username'], 'string', 'max' => 255],
            [['username'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'balance' => 'Balance',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return boolean
     */
    public function transfer($username, $sum)
    {
        $user = self::findByUsername($username);

        if (!$user) {
            $user = new self([
                'username' => $username,
            ]);

            if (!$user->save()) {
                return false;
            }
        }

        $transaction = Yii::$app->db->beginTransaction(Transaction::READ_COMMITTED);

        try {
            $this->updateAttributes([
                'balance' => round($this->balance - $sum, 2),
            ]);

            $user->updateAttributes([
                'balance' => round($user->balance + $sum, 2),
            ]);

            (new BalanceHistory([
                'from_user_id' => $this->id,
                'to_user_id' => $user->id,
                'sum' => round($sum, 2),
            ]))->save();

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();

            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBalanceHistories()
    {
        return $this->hasMany(BalanceHistory::className(), ['from_user_id' => 'id']);
    }
}
