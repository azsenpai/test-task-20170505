<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * RegisterForm is the model behind the register form.
 */
class RegisterForm extends Model
{
    public $username;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => User::className()],
        ];
    }

    /**
     * Register and logs in a user using the provided username
     * @return bool whether the user is logged in successfully
     */
    public function register()
    {
        if ($this->validate()) {
            $user = new User([
                'username' => $this->username,
            ]);

            if ($user->save()) {
                return Yii::$app->user->login($user);
            }
        }

        return false;
    }
}
