<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */

$this->title = 'Test Task';
?>
<div class="site-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'balance:decimal',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{transfer}',
                'buttons' => [
                    'transfer' => function ($url, $model, $key) {
                        if (Yii::$app->user->isGuest) {
                            return '';
                        }

                        return Html::a('transfer', ['transfer', 'username' => $model->username], ['class' => 'btn btn-primary']);
                    },
                ],
                'options' => ['width' => 100],
            ],
        ],
    ]); ?>
</div>