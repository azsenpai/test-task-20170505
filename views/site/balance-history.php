<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Balance History';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-balance-history">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Your balance: <b><?= Yii::$app->user->identity->balance ?></b></p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'fromUser.username',
                'label' => 'From',
            ],

            [
                'attribute' => 'toUser.username',
                'label' => 'To',
            ],

            'sum:decimal',

            'created_at:datetime',
        ],
    ]); ?>
</div>
