<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;

$this->title = 'Transfer';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-transfer">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->getFlash('transfer-success')): ?>
        <?= Alert::widget([
            'options' => ['class' => 'alert-success'],
            'body' => 'Transfer completed successfully',
        ]) ?>
    <?php endif ?>

    <?php if (Yii::$app->session->getFlash('transfer-error')): ?>
        <?= Alert::widget([
            'options' => ['class' => 'alert-danger'],
            'body' => 'Something goes wrong =(',
        ]) ?>
    <?php endif ?>

    <p>Your balance: <b><?= Yii::$app->user->identity->balance ?></b></p>

    <p>Please fill out the following fields to transfer:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'transfer-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'username')->textInput([
            'value' => Yii::$app->request->get('username'),
        ]) ?>

        <?= $form->field($model, 'sum')->textInput() ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Transfer', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
</div>
