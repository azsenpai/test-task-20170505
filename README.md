USING
-----
1. `git clone https://coder_iitu@bitbucket.org/coder_iitu/test-task-20170505.git`

2. `cd test-task-20170505`

3. `composer install`

4. `yii migrate/up`

5. `php -S localhost:8080 -t web`

6. Check the address http://localhost:8080

DB schema
---------
![schema.png](https://bitbucket.org/repo/7EEpXE8/images/3662263931-schema.png)