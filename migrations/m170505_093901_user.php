<?php

use yii\db\Migration;

/**
 *
 */
class m170505_093901_user extends Migration
{
    const TABLE_NAME = 'user';

    /**
     *
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'balance' => $this->double()->notNull()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createIndex(
            sprintf('uq-%s-username', self::TABLE_NAME),
            self::TABLE_NAME,
            'username',
            true
        );
    }

    /**
     *
     */
    public function down()
    {
        $this->dropIndex(
            sprintf('uq-%s-username', self::TABLE_NAME),
            self::TABLE_NAME
        );

        $this->dropTable(self::TABLE_NAME);
    }
}
