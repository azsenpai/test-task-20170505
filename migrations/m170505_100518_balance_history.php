<?php

use yii\db\Migration;

/**
 *
 */
class m170505_100518_balance_history extends Migration
{
    const TABLE_NAME = 'balance_history';

    /**
     *
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'from_user_id' => $this->integer()->notNull(),
            'to_user_id' => $this->integer()->notNull(),
            'sum' => $this->double()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(
            sprintf('fq-%s-from_user_id', self::TABLE_NAME),
            self::TABLE_NAME,
            'from_user_id',
            'user',
            'id'
        );

        $this->addForeignKey(
            sprintf('fq-%s-to_user_id', self::TABLE_NAME),
            self::TABLE_NAME,
            'to_user_id',
            'user',
            'id'
        );

        $this->createIndex(
            sprintf('idx-%s-from_user_id', self::TABLE_NAME),
            self::TABLE_NAME,
            'from_user_id',
            false
        );

        $this->createIndex(
            sprintf('idx-%s-to_user_id', self::TABLE_NAME),
            self::TABLE_NAME,
            'to_user_id',
            false
        );
    }

    /**
     *
     */
    public function down()
    {
        $this->dropForeignKey(
            sprintf('fq-%s-from_user_id', self::TABLE_NAME),
            self::TABLE_NAME
        );

        $this->dropForeignKey(
            sprintf('fq-%s-to_user_id', self::TABLE_NAME),
            self::TABLE_NAME
        );

        $this->dropIndex(
            sprintf('idx-%s-from_user_id', self::TABLE_NAME),
            self::TABLE_NAME
        );

        $this->dropIndex(
            sprintf('idx-%s-to_user_id', self::TABLE_NAME),
            self::TABLE_NAME
        );

        $this->dropTable(self::TABLE_NAME);
    }
}
